//Harmonic Oscillator
//Debug flags: -DDEBUG_MODE_TERMINAL, -DDEBUG_MODE_LOGFILE, -DFIXED_SEED

#include <vector>
#include <queue>
#include <utility>
#include <iostream>
#include <iterator>
#include <iomanip>
#include <fstream>
#include <filesystem>
#include <string>
#include <ctime>
#include <chrono>
#include <random>
#include <cmath>
#include <algorithm>
#include <omp.h>

//-----

std::string program_version = "Harmonic_Oscillator_1.1";

//-----




int PathSize;											// Number of Path segments
std::string PathSizeString;								// Number of Path segments (string)
double omega_hat;										// omega_hat
double omega_hat_inverse;								// 1/omega_hat
int n_run;												// Number of runs
int n_count {0};										// Current run
int expectation_period;									// Number of updates between calculation of expectation values
int append;												// Directory name appendix
std::string appendString;								// Directory name Appendix (string)
std::string directoryname_pre;							// Directory name (prefix)
std::string directoryname;								// Directory name
std::string logfilepath;								// Filepath (log)
std::string positionfilepath;							// Filepath (position)
std::string correlationfilepath;						// Filepath (correlation function)
std::string configfilepath;								// Filepath (config)
std::string finalconfigfilepath;						// File path (final config)
auto start = std::chrono::system_clock::now();			// Start time
std::time_t start_time;									// Start time
double beta_increments_distance;						// Distance between each beta increment
std::vector <double> PathVector;						// Vector containing the path segments
std::random_device randomdevice;						// Creates random device to seed PRNGs
std::ofstream datalog;									// Output stream to save data
std::ofstream positionlog;								// Output stream to save position/entries of vectors
std::ofstream correlationlog;							// Output stream to save correlation function
double action {0};

//-----

using std::cin;
using std::cout;
using std::endl;
using std::to_string;

//-----

void Configuration()
{
	/*cout << "\n" << "Bitte beta (Anfangswert) eingeben:";
	cin >> beta;
	cout << "\n" << "Bitte n_beta_increments eingeben:";
	cin >> n_beta_increments;*/
	cout << "\n" << "Please enter omega_hat: ";
	cin >> omega_hat;
	omega_hat_inverse = 1/omega_hat;
	cout << "\n" << "Please enter number of path segments: ";
	cin >> PathSize;
	cout << "\n" << "Please enter number of runs n_run: ";
	cin >> n_run;
	cout << "\n" << "Please enter expectation_period: ";
	cin >> expectation_period;
	//cout << "\n" << "Beta ist " << beta << ".\n";
	//cout << "Die gewählte Gittergröße ist " << PathSize << ", n_run ist " << n_run << " und expectation_period ist " << expectation_period << ".\n";
	//cout << "PathSizeHalf_d ist " << PathSizeHalf_d << " und PathSizeHalf ist " << PathSizeHalf << ".\n";
}

//-----

void SaveParameters(std::string filename, std::string starttimestring)
{
	datalog.open(filename, std::fstream::out | std::fstream::app);
	datalog << program_version << "\n";
	datalog << "logfile\n \n";
	#ifdef DEBUG_MODE_TERMINAL
	datalog << "DEBUG_MODE_TERMINAL\n";
	#endif
	datalog << starttimestring << "\n";
	datalog << "Pathsize = " << PathSize << "\n";
	datalog << "n_run = " << n_run << "\n";
	datalog << "expectation_period = " << expectation_period << "\n";
	datalog.close();
}

//-----

void CreateFiles()
{
	PathSizeString = to_string(PathSize);
	directoryname_pre = "HarmonicOscillator_N=" + PathSizeString;
	directoryname = directoryname_pre;
	append = 1;

	while (std::filesystem::exists(directoryname) == true)
	{
		appendString = to_string(append);
		directoryname = directoryname_pre + " (" + appendString + ")";
		++append;
	}

	std::filesystem::create_directory(directoryname);
	cout << "\n\n" << "Verzeichnis \"" << directoryname << "\" erstellt.\n";
	logfilepath = directoryname + "/log.txt";
	positionfilepath = directoryname + "/position.txt";
	correlationfilepath = directoryname + "/correlation.txt";
	configfilepath = directoryname + "/first_config.txt";
	finalconfigfilepath = directoryname + "/final_config.txt";
	cout << "Dateipfad (log): \t" << logfilepath << "\n";
	cout << "Dateipfad (position): \t" << positionfilepath << "\n";
	cout << "Dateipfad (config): \t" << configfilepath << "\n";
	cout << "Dateipfad (finalconfig): \t" << finalconfigfilepath << "\n \n";
	#ifdef DEBUG_MODE_TERMINAL
	cout << "DEBUG_MODE_TERMINAL\n \n";
	#endif	

	//-----
	//Writes parameters to files

	std::time_t start_time = std::chrono::system_clock::to_time_t(start);
	std::string start_time_string = std::ctime(&start_time);

	//logfile

	SaveParameters(logfilepath, start_time_string);

	//configfile

	//SaveParameters(configfilepath, start_time_string);

	//final configfile

	//SaveParameters(finalconfigfilepath, start_time_string);
}

//-----

void ColdStart(std::vector<double>& PathVector)
{
	PathVector.resize(PathSize);
	std::fill(PathVector.begin(), PathVector.end(), 0);

	for (int a = 0; a < PathSize; ++a)
	{
		cout << "PathVector = " << PathVector[a] << "\t (ColdStart)\n";
	}
}

//-----

void HotStart(std::vector<double>& PathVector)
{	
	PathVector.resize(PathSize);

	#ifdef FIXED_SEED
	std::mt19937 generator_h(1);
	#else
	std::mt19937 generator_h(randomdevice());
	#endif
	std::uniform_real_distribution<double> distribution_hotstart(-1.0,1.0);


	auto gen = [&distribution_hotstart, &generator_h](){return distribution_hotstart(generator_h);};
	std::generate(PathVector.begin(), PathVector.end(), gen);

	for (int a = 0; a < PathSize; ++a)
	{
		cout << "PathVector = " << PathVector[a] << "\t (HotStart)\n";
	}
	cout << "Size of Vector = " << PathVector.size();
}

//-----

void MetropolisAlgorithm(std::vector<double>& PathVector)
{
	double PathSuggestion;
	double PrevDiffOld;
	double NextDiffOld;
	double PrevDiffSuggestion;
	double NextDiffSuggestion;
	double DeltaAction;

	double Action {0};
	double Position {0};
	std::vector<double> Correlation_Function;
	Correlation_Function.resize(PathSize);

	#ifdef FIXED_SEED
	std::mt19937 generator_suggestion(2);
	#else
	std::mt19937 generator_suggestion(randomdevice());
	#endif
	std::uniform_real_distribution<double> distribution_suggestion(-1.0,1.0);

	#ifdef FIXED_SEED
	std::mt19937 generator_prob(3);
	#else
	std::mt19937 generator_prob(randomdevice());
	#endif
	std::uniform_real_distribution<double> distribution_prob(0,1.0);

	datalog.open(logfilepath, std::fstream::out | std::fstream::app);
	positionlog.open(positionfilepath, std::fstream::out | std::fstream::app);
	correlationlog.open(correlationfilepath, std::fstream::out | std::fstream::app);

	while (n_count < n_run)
	{
		for (int a = 0; a < PathSize; ++a)
		{
			PathSuggestion = PathVector[a] + distribution_suggestion(generator_suggestion);
			PrevDiffOld = std::pow((PathVector[a] - PathVector[( a + PathSize - 1) % PathSize]), 2);
			NextDiffOld = std::pow((PathVector[(a + 1) % PathSize] - PathVector[a]), 2);
			PrevDiffSuggestion = std::pow((PathSuggestion - PathVector[( a + PathSize - 1) % PathSize]), 2);
			NextDiffSuggestion = std::pow((PathVector[(a + 1) % PathSize] - PathSuggestion), 2);
			DeltaAction = omega_hat_inverse * ( NextDiffSuggestion + PrevDiffSuggestion - NextDiffOld - PrevDiffOld) + omega_hat * (std::pow(PathSuggestion, 2) - std::pow(PathVector[a], 2));
			double p = std::min(exp(-DeltaAction), 1.0);
			double q = distribution_prob(generator_prob);
			if (q < p)
			{
				PathVector[a] = PathSuggestion;
				//cout << "Suggestion accepted \n";
			}
			/*else
			{
				cout << "Suggestion rejected \n";
			}*/
		}
		if (n_count%expectation_period == 0)
		{
			Action = 0;
			Position = 0;
			for (int a = 0; a < PathSize; ++a)
			{
				Action += omega_hat_inverse * std::pow(PathVector[(a + 1) % PathSize], 2) - omega_hat * std::pow(PathVector[a], 2);
			}
			datalog << "Step " << n_count << "\n";
			datalog << "Action: " << Action << "\n" << endl;
			std::copy(PathVector.begin(), std::prev(PathVector.end()), std::ostream_iterator<double>(positionlog, ", "));
			positionlog << PathVector.back() << "\n";
			for (int a = 0; a < PathSize; ++a)
			{
				Correlation_Function[a] = PathVector[0] * PathVector[a];
			}
			std::copy(Correlation_Function.begin(), std::prev(Correlation_Function.end()), std::ostream_iterator<double>(correlationlog, ", "));
			correlationlog << Correlation_Function.back() << "\n";
		}
		n_count++;
	}
	datalog.close();
	positionlog.close();
	correlationlog.close();
}

//-----

int main()
{
	Configuration();
	HotStart(PathVector);
	//ColdStart(PathVector);
	CreateFiles();
	MetropolisAlgorithm(PathVector);
}